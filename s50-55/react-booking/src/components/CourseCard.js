// import { useState , useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({ courseProp }) {

    console.log(courseProp);

    // Deconstruct the course properties into their own variables
    const {_id,name, description, price } = courseProp;

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

    // Syntax: const [getter, setter] = userState(inititalGetterValue)
    // const [count, setCount] = useState(0);

    // "seats" will hold the current value of the available seats for the course
    // "setSeats" is used to update the value of the seats state variable
    // const [seats, setSeats] = useState(30);
    // const [isOpen, setIsOpen] = useState(false);

    // Function that keeps track of the enrollees for a course
    // By default JavaScript is synchronous, so it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
    // The setter function for useStates is asynchronous allowing it to execute separately from other codes in the program
    // The "setCount" function is being executed while the "console.log" is already completed resulting in the value to be displayed in the console to be behind by one count

    // ACTIVITY S51
    // function enroll() {
    //     if (seats > 0) {

    //         // Add enrollees
    //         setCount(count + 1);

    //         // Deduct from available seats
    //         setSeats(seats - 1);
    //         console.log("Enrollees: " + count);
            
    //     } else {
    //         alert("No more seats.");
    //         console.log("Enrollment limit reached");
    //     }
    // }

    // function enroll() {
    //     setCount(count + 1);
    //     console.log("Enrollees: " + count);
    //     setSeats(seats - 1);
    //     console.log("Seats: " + count);
    // }

    // useEffect(() => {
    //     if(seats === 0) {
    //         setIsOpen(true);
    //     }
    // }, [seats]);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                <Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    )
}


/*
    S51 ACTIVITY:
        1. Create a seats state in the CourseCard component and set the initial value to 10.
        2. For every enrollment, deduct one to the seats.
        3. If the seat reaches zero do the following:
        - Do not add to the count.
        - Do not deduct to the seats.
        - Show an alert that says "No more seats.
*/