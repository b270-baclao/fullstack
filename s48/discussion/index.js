// console.log("Hi there!");

// Mock database
let posts = [];

// Post ID
let count = 1;

// Add post
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	// Prevents the page from loading
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	count++;

	alert("Successfully added");
	showPosts(posts);

})

// Show Post
const showPosts = (posts) => {

	let postEntries = "";

	posts.forEach((post) => {
		console.log(posts);

		// We can assign HTML elements in JS variables
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}"> ${post.title} </h3>
				<p id="post-body-${post.id}"> ${post.body} </p>
				<button onclick="editPost(${post.id})"> Edit </button>
				<button onclick="deletePost(${post.id})"> Delete </button>
			</div>
		`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit post
// This will trigger an event that will update a certain post upon clicking the edit button

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
}

// Update post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

    e.preventDefault();

    for(let i = 0; i < posts.length; i++) {

    	// Loops through every post in the "posts" array starting from the first element/index 0
        if(posts[i].id == document.querySelector("#txt-edit-id").value) {

            posts[i].title = document.querySelector("#txt-edit-title").value;
            posts[i].body = document.querySelector("#txt-edit-body").value

            showPosts(posts)
            alert("Successfully updated!");
            break
        }
    }
});

// S48 Activity for DELETE 

// Delete post
// This will trigger an event that will delete a certain post upon clicking the delete button

// [Delete using filter()]

// const deletePost = (id) => {
//  	// Remove the post from the posts array
// 	posts = posts.filter(post => post.id !== id);

//   	// Remove the post element from the DOM
// 	const postElement = document.getElementById(`post-${id}`);
// 	if (postElement) {
// 		postElement.remove();
// 	}
// };


// [Delete using findIndex() and splice()]

const deletePost = (id) => {
  	// Find the index of the post with the provided ID
	const index = posts.findIndex(post => post.id === id);

 	// Check if the post exists in the array
	if (index !== -1) {

    	// Ask for confirmation before deleting the post
		const confirmDelete = confirm("Are you absolutely, positively, without a doubt, sure you want to delete this post? Remember, once it's gone, you can't blame it on the dog!");

		if (confirmDelete) {
      		// Remove the post from the array using splice()
			posts.splice(index, 1);

      		// Remove the post element from the DOM
			const postElement = document.getElementById(`post-${id}`);
			if (postElement) {
				postElement.remove();
			}

     		// Display a success message
			alert("Congratulations! The post has been deleted. Farewell, dear post!");
		}
	}
};








