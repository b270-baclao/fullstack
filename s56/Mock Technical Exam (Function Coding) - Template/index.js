/*
    1. 
        - Check first whether the letter is a single character.
        - If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        - If letter is invalid, return undefined.
*/

function countLetter(letter, sentence) {
  let result = 0;

  // Check first whether the letter is a single character.
  if (typeof letter === 'string' && letter.length === 1) {
    // Iterate over each character in the sentence and check if it matches the letter.
    for (let i = 0; i < sentence.length; i++) {
      if (sentence[i] === letter) {
        result++;
      }
    }
    return result;
  } else {
    return undefined; // Return undefined if the letter is not a single character.
  }
}


/*
    2. 
        - An isogram is a word where there are no repeating letters.
        - The function should disregard text casing before doing anything else.
        - If the function finds a repeating letter, return false. Otherwise, return true.
*/

function isIsogram(text) {
  // Convert the text to lowercase to disregard text casing
  const lowercaseText = text.toLowerCase();
  
  // Create an empty object to keep track of letters
  const letterCount = {};
  
  // Iterate over each letter in the text
  for (let i = 0; i < lowercaseText.length; i++) {
    const letter = lowercaseText[i];
    
    // Check if the letter is already in the object
    if (letterCount[letter]) {
      // If the letter is already in the object, it's a repeating letter
      return false;
    }
    
    // If the letter is not in the object, add it with a count of 1
    letterCount[letter] = 1;
  }
  
  // If no repeating letters are found, it's an isogram
  return true;
}


/*
    3. 
        - Return undefined for people aged below 13.
        - Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        - Return the rounded off price for people aged 22 to 64.
        - The returned value should be a string.
*/

function purchase(age, price) {
  // Return undefined for people aged below 13.
  if (age < 13) {
    return undefined;
  }

  // Return the discounted price for students aged 13 to 21 and senior citizens. (20% discount)
  if (age >= 13 && age <= 21 || age >= 65) {
    let discountedPrice = price * 0.8;
    return discountedPrice.toFixed(2);
  }

  // Return the original price for people aged 22 to 64.
  return price.toFixed(2);
}


/*
    4. 
        Find categories that has no more stocks.
        The hot categories must be unique; no repeating categories.

        The passed items array from the test are the following:
        { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

        The expected output after processing the items array is ['toiletries', 'gadgets'].

        Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
*/

function findHotCategories(items) {
  let hotCategories = [];

  // Iterate over the items array
  for (let i = 0; i < items.length; i++) {
    // Check if the stocks of the current item are 0
    // and if the category of the current item is not already in hotCategories
    if (items[i].stocks === 0 && !hotCategories.includes(items[i].category)) {
      // If the conditions are met, add the category to hotCategories
      hotCategories.push(items[i].category);
    }
  }

  // Return the array of hot categories
  return hotCategories;
}



/*
    5.  
        Find voters who voted for both candidate A and candidate B.

        The passed values from the test are the following:
        candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

        The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
        Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
*/


function findFlyingVoters(candidateA, candidateB) {
  let flyingVoters = [];

  // Iterate over the candidateA array
  for (let i = 0; i < candidateA.length; i++) {
    // Check if the current ID in candidateA is present in candidateB
    if (candidateB.includes(candidateA[i])) {
      // If the ID is found in both arrays, add it to the flyingVoters array
      flyingVoters.push(candidateA[i]);
    }
  }

  // Return the array of flying voters
  return flyingVoters;
}


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};