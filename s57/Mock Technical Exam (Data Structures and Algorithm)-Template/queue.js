let collection = [];

function enqueue(element) {
    collection.push(element);
    return collection;
}

function print() {
    return collection;
}

function dequeue() {
  collection.shift();
  return collection;
}


function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

// Implement the remaining queue functions here

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
