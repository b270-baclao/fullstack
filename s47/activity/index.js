// console.log("Hey there!")

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

function FullName() {
  const firstName = txtFirstName.value;
  const lastName = txtLastName.value;

  spanFullName.innerHTML = `${firstName} ${lastName}`;
}

txtFirstName.addEventListener('keyup', FullName);
txtLastName.addEventListener('keyup', FullName);

/*
	- The "FullName" function is called whenever there's a keyup event on either the txtFirstName or txtLastName input fields. 

	- It retrieves the values of both fields and sets the innerHTML of spanFullName to display the full name.
*/